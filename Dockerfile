# Base image of node
FROM node:14-alpine

# Producction
ENV NODE_ENV=production
ENV DB_connection=mongodb+srv://dbProjectDw:53UauHJhTBXAR7Q@cluster0.e4nbp.mongodb.net/dw_database?retryWrites=true&w=majority

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY . /usr/src/app/

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 3000
RUN chown -R node /usr/src/app
USER node
CMD [ "npm", "start" ]
