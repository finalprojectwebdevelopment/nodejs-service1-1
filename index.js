const express = require('express');
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const app = express();
const cors = require('cors');
require('dotenv/config')

//import routes
const putsRoutes = require('./src/Routes/Update');

//Middleware
app.use(cors());
app.use(bodyparser.json());


//routes
app.use('/product/Update', putsRoutes);


//DbConnection
mongoose.connect(process.env.DB_connection, { useNewUrlParser: true, useUnifiedTopology: true });


//Running port
app.listen(3011);

// ------------------ Eureka Config --------------------------------------------

const Eureka = require('eureka-js-client').Eureka;

const eureka = new Eureka({
  instance: {
    app: 'nodejsrest1',
    hostName: 'localhost',
    ipAddr: 'ip',
    statusPageUrl: 'http://ip:3011',
    instanceId: 'ip:nodejsrest1:3011',
    status: 'true',
    port: {
      '$': 3011,
      '@enabled': 'true',
    },
    vipAddress: 'nodejsrest1',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    }
  },
  eureka: {
    registerWithEureka: true,
    fetchMetadata: true,
    host: 'localhost',
    port: 8585,
    servicePath: '/eureka/apps/',
    hostName: 'localhost'
  }
});
eureka.logger.level('debug');
eureka.start(function(error){
  console.log(error || 'complete');
});

//---------------Spring-Cloud-Gateway----------------
